La aplicación, llamada Futboleros Go, tendrá como motor principal el
coleccionismo de los jugadores de fútbol de La Liga, la primera división de
fútbol española. Los jugadores aparecerán en un mapa y tendremos que
acercarnos para conseguirlos, solo los podremos desbloquear si cumplimos
algún requisito.
---
Además en la app se podrá ver información de la clasificación actual, así
como datos sobre los equipos. También se podrán consultar datos de los
jugadores, pero únicamente si los hemos conseguido.
---
Estará orientada al desenvolvimiento de un entregable.
---
Orientada a cualquier persona que le guste el coleccionismo y el fútbol.
Se podría ver como la evolución de los clásicos cromos de jugadores de fútbol.
---
En la actualidad creo que no existe ninguna app que trate exactamente
lo que trata esta, existen muchas apps de coleccionismo a través de
geolocalización y apps en las que coleccionas jugadores. Pero no he
encontrado ninguna que junte ambas características.
---
Podría llegar a comercializarse si las monedas utilizadas para “abrir” los
sobres de los jugadores, que de forma normal se obtendrían andando, se
pudiesen obtener pagando dinero real o viendo anuncios. Y cada año hay una
nueva temporada de futbol, por lo que se guardaría la colección actual y se
empezaría otra vez a coleccionar.
---
El objetivo principal que pretende alcanzar el proyecto es el de actualizar
el ya comentado clásico coleccionismo de cromos físicos de jugadores.
El alcance inicial de la app será a nivel global pero únicamente de los
jugadores de La Liga. En el futuro se podrían añadir los jugadores de más ligas
como la inglesa, la francesa o la italiana.
---
Crear un modelo entidad/relación, un modelo relacional… Y una vez
esquematizado y estructurado, será necesario crear una base de datos
completa, con los 495 jugadores de La Liga. Y una vez creada la base de datos,
diseñado el mapa y pensado el sistema de aparición de jugadores. Se podrá
empezar a programar.
---
Las tecnologías que se usarán serán Android Studio como IDE en
lenguaje Java para crear la app, las APIs de Google Maps para la gestión del
mapa y la base de datos será SQLite. Si hay tiempo se crearán animaciones en
Blender y implementadas en Android a través de SceneForm.
---
