### 1. Enumerade as funcionalidades que vai ofrecer a vosa aplicación informática, é dicir, que tarefas vai realizar. Cada unha delas é un caso de uso. Non é necesario que fagades os diagramas.

Mostras datos de La Liga, de los jugadores, llevar un control de la cantidad de jugadores que ya han sido conseguidos y el propio juego (Andar por el mundo y obeter dichos jugadores)

### 2. Facede un deseño de compoñentes e despregue, é dicir, cada "cubo" é un elemento físico, p.e. un computador. Dentro de cada "cubo" indicades os elementos software que se instalan, p.e. Java. Tendes un exemplo no "Repositorio" no GitLab, aplicable ós proxectos que realiza o alumnado de DAM con máis frecuencia.

![Diseño](doc/img/apps.png)

### 3. Facede un deseño da interface de usuario, é dicir, unha descrición das ventanas que haberá e como navegar entre elas. Podedes facer un esquema simple que iredes completando cando vaiades creando cada "prototipo".

_Pantalla Principal_ - (Play, Coleccion, Clasificación)

_Play_ - El propio juego.

_Coleccion_ - Los 495 jugadores en una lista.

_Clasificación_ - Los 20 equipos de la liga. (Si clicas en un equipo puedes ver sus datos y su plantilla con sus respectivos jugadores.)

### 4. Un esquema da BD, no caso de que a haxa, ou descrición de ficheiros JSON, se é o caso, etc.

![Esquema](doc/img/esquema.PNG)
