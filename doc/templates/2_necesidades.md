## Xustificación das necesidades detectadas que cubre o sistema a desenvolver

**1. Describe o problema ou a necesidade.**

    Ofrecer una aplicación de coleccionismo a los fans del fútbol.
**2. Por que é necesaria a posta en marcha dun proxecto que aborde dita necesidade?**

    Porque el coleccionismo necesita evolucionar con las tecnologías, y esta app trata de remodelar el coleccionismo de cromos de jugadores de fútbol.
**3. Cal é o obxectivo xeral que persegue o proxecto?**

    Entretener a los fans del coleccionismo y que salgan a la calle.
### 4. Responde a estas preguntas concretas:

**4.1. Como se pode responder a esta necesidade?**

    Realizando una aplicación que junte coleccionismo y fútbol.
**4.2. Que pode facerse para cambiar este estado de cousas?** 

    Trabajar en una aplicación que de una nueva forma de entretenimiendo a los futboleros y que a su vez les haga dar un paseo.
**4.3. Como podemos contribuír desde a nosa situación a que o problema se resolva?**

    Invirtiendo tiempo en la creacicón de la app para tenerla lo antes posible y que la gente pueda disfrutar de ella.
**4.4. Que medios, actividades e recursos van poñer en xogo?**

    Mi tiempo.
**4.5. Que actividades se van realizar?**

    La creación de una base de datos con todos los jugadores de La Liga, la propia aplicación en Android Studio y la documentación de los equipos.
**4.6. Que metodoloxía se vai empregar para levar a cabo o traballo?**

    Primero se tratará de diseñar y testear las diferentes tecnologías a utilizar. Y una vez esté todo organizado y listo se pasará a la fase de desarrollo real de la app.
**4.7. Que persoas serían precisas para realizar o proxecto con éxito?**

    Yo unicaménte.
**4.8 Con canto tempo se conta?**

    2 meses.
**4.9 Canto tempo se necesita?**

    1 mes.
## Posibilidades de comercialización (viabilidade, competidores…)

### 1.Viabilidade.

**1.1.Viabilidade técnica.**

**1.1.a) Será posible dispoñer dos recursos humanos e medios de produción necesarios (materias primas, maquinaria, instalacións…)?**
    
    Sí.
**1.1.b) Existe algún impedimento técnico que dificulte o proceso produtivo?**

    No, no existe ningún impedimento técnico que dificulte el proceso produtivo.
### 1.2.Viabilidade económica

**1.2.a) Os beneficios do proxecto son superiores aos custos?**
    
    Si se comercializa bien la app sí, podrían obtenerse un tipo de divisa para obtener más rápido los jugadores.
**1.2.b) As perdas poden cubrirse vía financiamento (por parte da administración pública, con subvencións, etc)?**

    No haría falta de ayudas públicas parta financiar la app, créditos bancarios si fuera necesario.
### 2.Competencia.

**2.1. Identificación da competencia, as súas características e a súa posición no mercado.**

    No habría competencia directa, pero apps como Pokemon Go son similares y muy populares.
**2.2. Existencia de produtos/servizos substitutivos.**

    Toda app de coleccionismo a través de la geolocalización podría considerarse de tipo substitutivo.
## Ideas para a súa comercialización

### 1.Promoción.


**1.1.Técnicas elixidas (redes sociais, plataformas multimedia, páxina web, posicionamento web SEO, patrocinios, participación en eventos, prácticas de responsabilidade social corporativa, outras).**

    Redes sociales principalmente, y a través de contratos con creadores de contenido si se diera la oportunidad.
**1.2.Xustifica a elección.**

    Son las formas a través de las cuales se accede de forma más efectiva al grupo de población al que va dirigida la app.
### 2.Modelo de negocio.


**2.1.Modelo elixido (Modelo de pago / Freemium (é de balde pero as funcionalidades extras son de pago) / In house (desenvolvementos a medida para contornos empresariais / De subscrición / Por publicidade / Outros)**

    Freemium.
**2.2. Xustifica a elección.**

    Se quiere que use la app el máximo de gente posible, para que ayudo a su popularización. Pero algún sistema de pago ayudaría a agilizar el proceso de coleccionismo de jugadores.
