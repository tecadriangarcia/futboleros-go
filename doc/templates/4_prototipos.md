## Prototipos

Como la mayor parte de la aplicación ya está hecha, los prototipos indicarán labores más documentales o de optimización.

### Prototipo 1 - 21/05/2021

. Documentar la separación de zonas y carga de datos.

. Optimizar la lectura del html con los datos de los equipos.

. Corregir bugs con respecto al ciclo de vida de las activitys en android, que daba problemas al realizar ciertas secuencias de acciones.

. Creada cuadrícula a mano en el Google Earth Pro para hacer pruebas respecto a las zonas y que además sea más visual su disposición.

. Creación de mapas personalizados con la herramienta [Google Cloud Platform](https://console.cloud.google.com/google/maps-apis/studio/styles?project=avanzada-5).

>**Mapa oscuro     - 	    60d7bcb0115bafe2** (Escogido final)

>Mapa claro      -       87cc19f2157727f0

### Prototipo 2 - 04/06/2021

. Debido a cambios en el html de lectura de estadísticas de los equipos. Se ha tenido que actualizar la lectura y procesado de los datos.

. Documentar el proceso de obtención de cromos

. Pruebas de funcionamiento en distintos dispositivos móviles diferentes, sin problemas.

. Pruebas y aprendizaje en Blender para una posible introducción de animaciones y realidad aumentada.

. Arreglados problemas surgidos por el fin de la liga. Tando de procesesado como de visualización de los datos.

. Añadido al circulo alrededor del avatar del usuario que cambié de color en función de la zona en la que estemos, para visualizar el cambio.

