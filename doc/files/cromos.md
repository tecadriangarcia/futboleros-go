## Cromos

Cada jugador tiene su respectivo cromo, en español y en inglés. Como hay 495 jugadores almacenados esto hace un total de 990 imágenes diferentes que han tenido que ser creadas. Aquí se explicará el proceso de creación y edición de estos cromos.

En el fichero [carga.md](doc/files/carga.md) se comenta cómo y donde se guardan los cromos.

### 1 - Obtener la imágen base.

Se comienza con una imagen como la siguiente. Todas las imágenes se obtuvieron de la web [futbin](https://www.futbin.com/).

La imagen no tiene fondo transparente y está en inglés.

![oblak](doc/img/cromo_1.PNG)

### 2 .- Eliminación del fondo.

Usando las acciones planificadas del Adobe Photoshop, podemos grabar una secuencia de acciones a realizar en los 495 cromos.

Seleccionamos el pixel 0, 0 con la varita mágica con poca tolerancia, borramos lo seleccionado y por último cortamos basando en pixeles transparentes. Y obtenemos el siguiente resultado para las 495 imágenes.

![oblak](doc/img/cromo_2.PNG)

### 3 .- Pasar a español.

Ya tenemos la mitad hecha. Faltan las versiones en español.

Esta es la parte más complicada y tediosa. Se tuvo que poner por encima de las imágenes obtenidas una capa que cambia las estádisticas y posición del jugador al español.

Como hay 17 posiciones y 6 colores de cromo diferentes. En total son unas 100 imágenes nuevas que hacen falta para hacer el trabajo. Se podría hacer de otra manera pero no quedaría tan bien.

A continuación se muestra la capa necesaria para pasar a un portero oro brillante al español más el resultado de juntarlo.

![oblak](doc/img/cromo_3.PNG)

![oblak](doc/img/cromo_4.PNG)

Después de realizar este proceso con las 495 imágenes solo una necesito de edición a mayores ([Aridane](https://es.wikipedia.org/wiki/Aridane_Hern%C3%A1ndez), jugador del osasuna, tiene el pelo muy afro y la capa para traducir lo tapaba).
