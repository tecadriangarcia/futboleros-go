## Zonas y Áreas del mapa

#### Introducción/Justificación

Las zonas del mapa son usadas para que, de **forma automática**, los **jugadores se desplieguen por el mapamundi** siguiendo un patrón y que los jugadores de los equipos mejor posicionados en la clasificación tengan menos zonas. Usando los decimales de la latitud y longitud consigo la zona en donde se ubican.

---
---

El mundo está dividido en **áreas** y cada una de estas en 100 **zonas**. Para saber en que área y zona se encuentra el jugador o cromo, se usa la **latitud** y **longitud**. Hay dos tipos de áreas, las pares y las impares.

Una zona es par si la suma de su primer decimal en longitud y su primer decimal en latitud es par. Por ejemplo:

Dadas las coordenadas: lat. 42,75º | lon. -8,17º

El primer decimal de la latitud es 7 y el primer decimal de la longitud es 1. La suma sería 8, por lo tanto el área es de tipo par. 

> Cuando la suma es impar, el tipo de área es impar.

La parte de las zonas es un poco más complicada. Como ya se indicó previamente, cada área tiene 100 zonas (10x10). Estas son necesarias para saber de que equipo serán los jugadores que aparezcan es dicha zona. Los equipos mejor posicionados en la clasificación de La Liga aparecerán en zonas menos abundantes, y los peor posicionados apareceran en las zonas que más aparecen.

Para saber en que zona (0, 1, 2, 3... 9) estamos. Primero necesitamos saber el tipo de área.

1. Si el área es **par**:

    Usamos la siguiente ecuación donde lat2 es el segundo decimal de la latitud y lon2 es el segundo decimal de la longitud.

    lat2 + lon2 = x

    nZona = x > 9 ? x - ((x - 9) * 2) : x

    Por ejemplo, usando las coordenadas de antes. lat. 42,75º | lon. -8,17º

        5 + 7 = 12

        nZona = 12 > 9 ? 12 - ((12 - 9) * 2) : 12

        nZona = 12 - ((12 - 9) * 2)

        nZona = 12 - 6

        nZona = 6

2. Si el área es **impar**:

    Usamos la siguiente ecuación donde, al igual que antes, lat2 es el segundo decimal de la latitud y lon2 es el segundo decimal de la longitud.

    lat2 - lon2 < 0 ? lat2 + [lon2-9] : lon2 + [lat2-9];

    Por ejemplo, usando las coordenadas. lat. -12,34º | lon. 20,28º

        nZona = 4 - 8 < 0 ? 4 + [8-9] : 8 + [4-9]

        nZona = 4 + [8-9]

        nZona = 4 + 1

        nZona = 5

Si analizamos la siguiente imagen, podemos ver varias cosas:

* Las **áreas** pares delimitadas con un borde rojo.
* Las **áreas** impares delimitadas con un borde amarillo.
* Las **zonas** de cada área. Podemos ver los dos patrones que crean las anteriores ecuaciones.
* La diferente cantidad de zonas. Las zonas blancas, o zonas 0, únicamente aparecen en dos ocasiones en cada área. Mientras que las zonas de color fucsia aparecen 18 veces.

![zonas](doc/img/zonas.png)

Si hacemos una última prueba real con las coordenadas marcadas, 42,91º/-8,11º. Que como se ve en la imagen sería el comienzo de una zona 2.

Es una área par ya que 9 + 1 = 10, por lo que usando la ecuación: 

    2 = 1 + 1

    nZona = 2 > 9 ? 2 - ((2 - 9) * 2) : 2

    nZona = 2

