# Carga y Actualización de los datos

## Equipos

La forma de actualizar los datos no es perfecta, ni mucho menos. Lo mejor sería usar consultas SQL hacia una base de datos, pero las bases de datos que llevan estadísticas de fútbol son muy caras.

Las estadísticas de los equipos cambian con el transcurso de la liga (partidos ganados, goles marcados...). Cada vez que se inicia la app se actualizan estos datos consultando la página web [**fcstats**](https://fcstats.com/table,primera-division-spain,19,1.php), donde tenemos información actualizada de La Liga.

Alguna información de los equipos en la base de datos como nombre o escudo no varían al actualizarse los datos.

El fichero **html** se analiza a través de **expresiones regulares**. Cuando encuentra un resultado se guarda, cuando todos los datos de un equipo han sido encontrados se crea un objeto Equipo y se añade a la **base de datos**.

Algunas expresiones usadas son: (Estas expresiones regulares, al ser usadas para web scraping, es necesario actualizarlas)

* Para encontrar un partido jugado o por jugar  **"title=\"(.*?)\">$"**
* Para saber si un partido es como local y obtener el nombre del rival  **NombreEquipo + "\\s-\\s(.*?)(\\s\\d\\d?:\\d\\d?)?$"**
* Para saber si un partido es como visitante y obtener el nombre del rival  **"(.*?)\\s-\\s"**
* Para saber el resultado de un partido  **"\\s(.*?):(.*?)$"**

Cuando se lee el fichero se añade a los equipos el número de [**zona**](doc/files/zonas.md) en donde aparecen sus jugadores.

## Jugadores

Los datos de los jugadores ya van directamente al instalar la app, ya que son **estadísticas globales** dadas al inicio de la temporada.

De los jugadores únicamente es necesario bajar de internet sus respectivos cromos.

Como la app está en inglés y en español, es necesario que los cromos estén en estos dos idiomas. Hay 495 jugadores, por lo que hacen falta 990 fotos.

Están almacenadas en **dos webs** creadas por mi, una para las fotos en [**español**](https://a19adriangces.weebly.com/) y otra para las fotos en [**inglés**](https://a19adriangcen.weebly.com/).

Se usan dos webs para mejor organización. Como cada jugador tiene asociado en la base de datos un **nombre de cromo**. Usando el idioma del teléfono para seleccionar la web + este nombre de cromo, tenemos la ruta para descargar la imagen.

Se descargan únicamente cuando se quieren consultar ya que son muchas imágenes y ocuparían demasiado espacio.
