package com.example.futbolerosgo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class Plantilla extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private String equipoSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plantilla);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        ArrayList<JugadorPlantilla> jugadoresPlantilla;
        if(getIntent().hasExtra("EQUIPO")) {
            String equipo = getIntent().getExtras().getString("EQUIPO");
            if(equipo == null) {
                jugadoresPlantilla = Main.baseDatos.getJugadoresPlantilla(equipoSave);
            } else {
                jugadoresPlantilla = Main.baseDatos.getJugadoresPlantilla(equipo);
            }
        } else {
            jugadoresPlantilla = Main.baseDatos.getJugadoresPlantilla();
        }
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewPlantilla);
        layoutManager = new LinearLayoutManager(this);
        mAdapter = new AdapterPlantilla(jugadoresPlantilla);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(getIntent().hasExtra("EQUIPO")) outState.putString("EQUIPOSAVE", getIntent().getExtras().getString("EQUIPO"));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(getIntent().hasExtra("EQUIPO")) equipoSave = savedInstanceState.getString("EQUIPOSAVE");
    }


}