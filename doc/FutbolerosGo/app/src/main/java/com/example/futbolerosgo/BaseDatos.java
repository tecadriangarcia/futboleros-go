package com.example.futbolerosgo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class BaseDatos extends SQLiteOpenHelper implements Serializable {

        public SQLiteDatabase sqlLiteDB;
        private Main main;
        public final static String NOME_BD = "futboleros_go";
        public final static int VERSION_BD = 1;
        private String TEAMS_FROM_ZONE = "SELECT idEquipo FROM equipo WHERE zona=?";
        private String PLAYER_FROM_ZONE = "SELECT idJugador FROM historial WHERE idEquipo=? ORDER BY RANDOM() LIMIT 1";
        private String INFO_FROM_PLAYER = "SELECT nombre, obtenido from jugador where idJugador=?";


        public BaseDatos(Main main) {
            super(main.getApplicationContext(), NOME_BD, null, VERSION_BD);
            this.main = main;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        public int getIdEquipo(String nombre) {
                Cursor cursor = sqlLiteDB.rawQuery("select idEquipo from equipo where nombre=?", new String[] {nombre});
                int idEquipo = 0;
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                idEquipo = cursor.getInt(0);
                                cursor.moveToNext();
                        }
                }
                return idEquipo;
        }

        public String getNombreEquipo(String idEquipo) {
                Cursor cursor = sqlLiteDB.rawQuery("select nombre from equipo where idEquipo=?", new String[] {idEquipo});
                String nombreEquipo = "";
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                nombreEquipo = cursor.getString(0);
                                cursor.moveToNext();
                        }
                }
                return nombreEquipo;
        }

        public void addTeamInfo(Equipo equipo, int zona) {
                int idEquipo = getIdEquipo(equipo.nombre);
                ContentValues datos = new ContentValues();
                datos.put("puntos", equipo.puntos);
                datos.put("posicion", equipo.posicion);
                datos.put("partidosJugados", equipo.partidosJugados);
                datos.put("partidosGanados", equipo.partidosGanados);
                datos.put("partidosEmpatados", equipo.partidosEmpatados);
                datos.put("partidosPerdidos", equipo.partidosPerdidos);
                datos.put("golesAFavor", equipo.golesAFavor);
                datos.put("golesEnContra", equipo.golesEnContra);
                datos.put("zona", zona);
                String condicionwhere = "idEquipo=?";
                String[] parametros = new String[]{String.valueOf(idEquipo)};
                sqlLiteDB.update("equipo", datos, condicionwhere, parametros);
                //addSiguientePartido(equipo.siguientePartido, idEquipo);
                for(UltimoPartido ultimoPartido : equipo.ultimosPartidos) {
                        addUltimoPartido(ultimoPartido, idEquipo);
                }
        }

        /*private void addSiguientePartido(SiguientePartido siguientePartido, int idEquipo) {
                ContentValues datos = new ContentValues();
                datos.put("idEquipo", idEquipo);
                datos.put("idEquipoRival", getIdEquipo(siguientePartido.equipoRival));
                datos.put("local", siguientePartido.local);
                sqlLiteDB.insert("siguientePartido", null, datos);
        }*/

        private void addUltimoPartido(UltimoPartido ultimoPartido, int idEquipo) {
                ContentValues datos = new ContentValues();
                datos.put("idEquipo", idEquipo);
                datos.put("idEquipoRival", getIdEquipo(ultimoPartido.equipoRival));
                datos.put("nPartido", ultimoPartido.nPartido);
                datos.put("local", ultimoPartido.local);
                datos.put("golesAFavor", ultimoPartido.golesAFavor);
                datos.put("golesEnContra", ultimoPartido.golesEnContra);
                sqlLiteDB.insert("ultimoPartido", null, datos);
        }

        public void deleteSpUp() {
                sqlLiteDB.delete("siguientePartido" ,null,null);
                sqlLiteDB.delete("ultimoPartido" ,null,null);
        }

        public ArrayList<JugadorPlantilla> getJugadoresPlantilla(String equipo) {
                ArrayList<JugadorPlantilla> jugadoresPlantilla = new ArrayList<>();
                Cursor cursor = sqlLiteDB.rawQuery("select idJugador from historial where idEquipo=?", new String[] {String.valueOf(getIdEquipo(equipo))});
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                Cursor cursorJugador = sqlLiteDB.rawQuery("select calidad, nombre, obtenido from jugador where idJugador=?", new String[] {cursor.getString(0)});
                                if (cursorJugador.moveToFirst()) {
                                        while (!cursorJugador.isAfterLast()) {
                                                jugadoresPlantilla.add(new JugadorPlantilla(
                                                        cursorJugador.getInt(0),
                                                        cursorJugador.getString(1),
                                                        cursorJugador.getInt(2),
                                                        cursor.getString(0)
                                                ));
                                                cursorJugador.moveToNext();
                                        }
                                }
                                cursor.moveToNext();
                        }
                }
                Collections.sort(jugadoresPlantilla, (jp1, jp2) -> {
                        if (jp1.getCalidadPlantilla() < jp2.getCalidadPlantilla()) return 1;
                        if (jp1.getCalidadPlantilla() > jp2.getCalidadPlantilla()) return -1;
                        return 0;
                });
                Collections.sort(jugadoresPlantilla, (jp1, jp2) -> {
                        if (jp1.getObtenidoPlantilla() < jp2.getObtenidoPlantilla()) return 1;
                        if (jp1.getObtenidoPlantilla() > jp2.getObtenidoPlantilla()) return -1;
                        return 0;
                });
                return jugadoresPlantilla;
        }

        public ArrayList<JugadorPlantilla> getJugadoresPlantilla() {
                ArrayList<JugadorPlantilla> jugadoresPlantilla = new ArrayList<>();
                Cursor cursor = sqlLiteDB.rawQuery("select calidad, nombre, obtenido, idJugador from jugador order by obtenido desc, calidad desc", new String[] {});
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                jugadoresPlantilla.add(new JugadorPlantilla(
                                        cursor.getInt(0),
                                        cursor.getString(1),
                                        cursor.getInt(2),
                                        cursor.getString(3)
                                ));
                                cursor.moveToNext();
                        }
                }
                return jugadoresPlantilla;
        }

        public ArrayList<String> getEquiposZona(int zona) {
                ArrayList<String> equiposZona = new ArrayList<>();
                Cursor cursor = sqlLiteDB.rawQuery("select nombre from equipo where zona=?", new String[] {String.valueOf(zona)});
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                equiposZona.add(cursor.getString(0));
                                cursor.moveToNext();
                        }
                }
                return equiposZona;
        }

        public ArrayList<EquipoClasificacion> getEquiposClasificacion() {
                ArrayList<EquipoClasificacion> equipos = new ArrayList<>();
                Cursor cursor = sqlLiteDB.rawQuery("select escudo, posicion, nombre, puntos, idEquipo from equipo order by posicion asc", new String[] {});
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                equipos.add(new EquipoClasificacion(
                                        main.getResources().getIdentifier(cursor.getString(0), "drawable", main.getApplicationContext().getPackageName()),
                                        cursor.getString(1),
                                        cursor.getString(2),
                                        cursor.getString(3)
                                ));
                                cursor.moveToNext();
                        }
                }
                return equipos;
        }

        public String getJugadoresColeccion(String equipo) {
                Cursor cursor = sqlLiteDB.rawQuery("select idJugador from historial where idEquipo=?", new String[] {String.valueOf(getIdEquipo(equipo))});
                int total = 0;
                int totalObtenidos = 0;
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                Cursor cursorJugador = sqlLiteDB.rawQuery("select obtenido from jugador where idJugador=?", new String[] {cursor.getString(0)});
                                if (cursorJugador.moveToFirst()) {
                                        while (!cursorJugador.isAfterLast()) {
                                                total += 1;
                                                if(cursorJugador.getInt(0) == 1) totalObtenidos += 1;
                                                cursorJugador.moveToNext();
                                        }
                                }
                                cursor.moveToNext();
                        }
                }
                return totalObtenidos + "/" + total;
        }

        public String getJugadoresColeccion() {
                int total = 0;
                int totalObtenidos = 0;
                Cursor cursor = sqlLiteDB.rawQuery("select obtenido from jugador", new String[] {});
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                total += 1;
                                if(cursor.getInt(0) == 1) totalObtenidos += 1;
                                cursor.moveToNext();
                        }
                }
                return totalObtenidos + "/" + total;
        }

        public Jugador getJugadorEstadisticas(String idJugadorDB) {
                Jugador jugador = null;
                Cursor cursor = sqlLiteDB.rawQuery("select cromo, nombre, fechaNacimiento, calidad, idPosicion from jugador where idJugador=?", new String[] {idJugadorDB});
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                jugador = new Jugador();
                                jugador.setCromo(cursor.getString(0));
                                jugador.setNombre(cursor.getString(1));
                                jugador.setFechaNacimiento(cursor.getString(2));
                                jugador.setCalidad(cursor.getInt(3));
                                jugador.setIdPosicion(cursor.getInt(4));
                                cursor.moveToNext();
                        }
                }
                return jugador;
        }

        public ArrayList<Integer> getJugadorEstadisticasSkills(String idJugadorDB, int posicion) {
                ArrayList<Integer> skills = new ArrayList<>();
                Cursor cursor = null;
                if(posicion == 1) {
                        cursor = sqlLiteDB.rawQuery("select * from portero where idJugador=?", new String[] {idJugadorDB});
                } else {
                        cursor = sqlLiteDB.rawQuery("select * from jugadorDeCampo where idJugador=?", new String[] {idJugadorDB});
                }
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                skills.add(cursor.getInt(1));
                                skills.add(cursor.getInt(2));
                                skills.add(cursor.getInt(3));
                                skills.add(cursor.getInt(4));
                                skills.add(cursor.getInt(5));
                                skills.add(cursor.getInt(6));
                                cursor.moveToNext();
                        }
                }
                return skills;
         }

        public Equipo getEquipoEstadisticas(int posicion) {
                Cursor cursor = sqlLiteDB.rawQuery("select nombre, puntos, posicion, partidosJugados, partidosGanados, partidosEmpatados, partidosPerdidos, golesAFavor, golesEnContra, zona, escudo, idEquipo from equipo where posicion=?", new String[] {String.valueOf(posicion)});
                Equipo equipo = null;
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                equipo = new Equipo();
                                equipo.nombre = cursor.getString(0);
                                equipo.puntos = cursor.getString(1);
                                equipo.posicion = cursor.getString(2);
                                equipo.partidosJugados = cursor.getString(3);
                                equipo.partidosGanados = cursor.getString(4);
                                equipo.partidosEmpatados = cursor.getString(5);
                                equipo.partidosPerdidos = cursor.getString(6);
                                equipo.golesAFavor = cursor.getString(7);
                                equipo.golesEnContra = cursor.getString(8);
                                equipo.zona = cursor.getString(9);
                                equipo.escudo = cursor.getString(10);
                                //equipo.siguientePartido = getSiguientePartido(cursor.getInt(11));
                                equipo.ultimosPartidos = getUltimosPartido(cursor.getInt(11));
                                cursor.moveToNext();
                        }
                }
                return equipo;
        }

        private ArrayList<UltimoPartido> getUltimosPartido(int idEquipo) {
                ArrayList<UltimoPartido> ultimosPartidos = new ArrayList<>();
                Cursor cursor = sqlLiteDB.rawQuery("select idEquipoRival, nPartido, local, golesAFavor, golesEnContra from ultimoPartido where idEquipo=? order by nPartido desc", new String[] {String.valueOf(idEquipo)});
                UltimoPartido ultimoPartido = null;
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                Cursor cursor2 = sqlLiteDB.rawQuery("select nombre, escudo from equipo where idEquipo=?", new String[] {cursor.getString(0)});
                                String equipoRival = "";
                                String escudo = "";
                                if (cursor2.moveToFirst()) {
                                        while (!cursor2.isAfterLast()) {
                                                equipoRival = cursor2.getString(0);
                                                escudo = cursor2.getString(1);
                                                cursor2.moveToNext();
                                        }
                                }
                                ultimoPartido = new UltimoPartido();
                                ultimoPartido.equipoRival = equipoRival;
                                ultimoPartido.nPartido = cursor.getString(1);
                                ultimoPartido.escudoEquipoRival = escudo;
                                ultimoPartido.local = !cursor.getString(2).equals("0");
                                ultimoPartido.golesAFavor = cursor.getString(3);
                                ultimoPartido.golesEnContra = cursor.getString(4);
                                ultimosPartidos.add(ultimoPartido);
                                cursor.moveToNext();
                        }
                }
                return ultimosPartidos;
        }

        /*private SiguientePartido getSiguientePartido(int idEquipo) {
                Cursor cursor = sqlLiteDB.rawQuery("select idEquipoRival, local from siguientePartido where idEquipo=?", new String[] {String.valueOf(idEquipo)});
                SiguientePartido siguientePartido = null;
                if (cursor.moveToFirst()) {
                        while (!cursor.isAfterLast()) {
                                Cursor cursor2 = sqlLiteDB.rawQuery("select nombre, escudo from equipo where idEquipo=?", new String[] {cursor.getString(0)});
                                String equipoRival = "";
                                String escudo = "";
                                if (cursor2.moveToFirst()) {
                                        while (!cursor2.isAfterLast()) {
                                                equipoRival = cursor2.getString(0);
                                                escudo = cursor2.getString(1);
                                                cursor2.moveToNext();
                                        }
                                }
                                siguientePartido = new SiguientePartido(equipoRival, !cursor.getString(1).equals("0"));
                                siguientePartido.escudoEquipoRival = escudo;
                                cursor.moveToNext();
                        }
                }
                return siguientePartido;
        }*/

        public String getRandomPlayerFromZone(int zona) {
                String[] args = new String[] {String.valueOf(zona)};
                Cursor datosConsulta = sqlLiteDB.rawQuery(TEAMS_FROM_ZONE, args);
                ArrayList<String> listaIdEquipos = new ArrayList<>();
                String salida = "";
                if (datosConsulta.moveToFirst()) {
                        while (!datosConsulta.isAfterLast()) {
                                listaIdEquipos.add(String.valueOf(datosConsulta.getInt(0)));
                                datosConsulta.moveToNext();
                        }
                }
                Collections.shuffle(listaIdEquipos);
                datosConsulta = sqlLiteDB.rawQuery(PLAYER_FROM_ZONE, new String[] {String.valueOf(listaIdEquipos.get(0))});
                int idJugador = 0;
                if (datosConsulta.moveToFirst()) {
                        while (!datosConsulta.isAfterLast()) {
                                idJugador = datosConsulta.getInt(0);
                                datosConsulta.moveToNext();
                        }
                }
                datosConsulta = sqlLiteDB.rawQuery(INFO_FROM_PLAYER, new String[] {String.valueOf(idJugador)});
                if (datosConsulta.moveToFirst()) {
                        while (!datosConsulta.isAfterLast()) {
                                int obtenido = datosConsulta.getInt(1);
                                String equipo = getNombreEquipo(listaIdEquipos.get(0));
                                if(obtenido == 0) {
                                        salida = main.getResources().getString(R.string.playerRandomGot1) + " \"" + datosConsulta.getString( 0) + "\" " + main.getResources().getString(R.string.playerRandomGot2) + " " + equipo;
                                        updateObtenido(idJugador);
                                } else {
                                        salida = main.getResources().getString(R.string.playerRandomGotGotten1) + " \"" + datosConsulta.getString( 0) + "\"" + main.getResources().getString(R.string.playerRandomGotGotten2) + " " + equipo + "" + main.getResources().getString(R.string.playerRandomGotGotten3);
                                }
                                datosConsulta.moveToNext();
                        }
                }
                return salida;
        }

        private void updateObtenido(int idJugador) {
                ContentValues datos = new ContentValues();
                datos.put("obtenido", 1);
                String condicionwhere = "idJugador=?";
                String[] parametros = new String[]{String.valueOf(idJugador)};
                sqlLiteDB.update("jugador", datos, condicionwhere, parametros);
        }

}
