package com.example.futbolerosgo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class AdapterPlantilla extends RecyclerView.Adapter<AdapterPlantilla.ViewHolderPlantilla> {

    private ArrayList<JugadorPlantilla> jugadoresPlantilla;

    public static class ViewHolderPlantilla extends RecyclerView.ViewHolder {

        public TextView textViewCPlantilla;
        public TextView textViewNPlantilla;
        public CheckBox checkBoxOPlantilla;
        public CardView cardViewPlantilla;

        public ViewHolderPlantilla(@NonNull View itemView) {
            super(itemView);
            textViewCPlantilla = itemView.findViewById(R.id.calidadPlantilla);
            textViewNPlantilla = itemView.findViewById(R.id.nombrePlantilla);
            checkBoxOPlantilla = itemView.findViewById(R.id.obtenidoPlantilla);
            cardViewPlantilla = itemView.findViewById(R.id.cardViewPlantilla);
        }
    }

    public AdapterPlantilla(ArrayList<JugadorPlantilla> jugadoresPlantilla) {
        this.jugadoresPlantilla = jugadoresPlantilla;
    }

    @NonNull
    @Override
    public ViewHolderPlantilla onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plantilla_card, parent, false);
        return new ViewHolderPlantilla(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderPlantilla holder, int position) {
        JugadorPlantilla jugadorPlantilla = jugadoresPlantilla.get(position);
        boolean obtenido = jugadorPlantilla.getObtenidoPlantilla() != 0;
        int calidad = jugadorPlantilla.getCalidadPlantilla();
        if(obtenido) holder.cardViewPlantilla.setOnClickListener(v -> Main.main.onClickCardViewPlantilla(jugadorPlantilla.getIdJugadorPlantilla()));
        if(calidad < 65) holder.cardViewPlantilla.setCardBackgroundColor(0xffe1b384);
        else if (calidad < 75) holder.cardViewPlantilla.setCardBackgroundColor(0xffd9d9d9);
        else holder.cardViewPlantilla.setCardBackgroundColor(0xffffdd4c);
        holder.textViewCPlantilla.setText(String.valueOf(calidad));
        holder.textViewNPlantilla.setText(jugadorPlantilla.getNombrePlantilla());
        holder.checkBoxOPlantilla.setChecked(obtenido);
    }

    @Override
    public int getItemCount() {
        return jugadoresPlantilla.size();
    }
}
