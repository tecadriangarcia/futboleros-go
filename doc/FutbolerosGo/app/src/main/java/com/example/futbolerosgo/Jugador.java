package com.example.futbolerosgo;

public class Jugador {

    private int idJugador;
    private String nombre;
    private String nacion;
    private String fechaNacimiento;
    private int calidad;
    private String cromo;
    private int idPosicion;
    private int obtenido;

    public Jugador() {

    }

    public Jugador(int idJugador, String nomnbre, String nacion, String fechaNacimiento, int calidad, String cromo, int idPosicion, int obtenido) {
        this.idJugador = idJugador;
        this.nombre = nomnbre;
        this.nacion = nacion;
        this.fechaNacimiento = fechaNacimiento;
        this.calidad = calidad;
        this.cromo = cromo;
        this.idPosicion = idPosicion;
        this.obtenido = obtenido;
    }

    public int getIdJugador() {
        return idJugador;
    }

    public void setIdJugador(int idJugador) {
        this.idJugador = idJugador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNacion() {
        return nacion;
    }

    public void setNacion(String nacion) {
        this.nacion = nacion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getCalidad() {
        return calidad;
    }

    public void setCalidad(int calidad) {
        this.calidad = calidad;
    }

    public String getCromo() {
        return cromo;
    }

    public void setCromo(String cromo) {
        this.cromo = cromo;
    }

    public int getIdPosicion() {
        return idPosicion;
    }

    public void setIdPosicion(int idPosicion) {
        this.idPosicion = idPosicion;
    }

    public int getObtenido() {
        return obtenido;
    }

    public void setObtenido(int obtenido) {
        this.obtenido = obtenido;
    }
}
