package com.example.futbolerosgo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.google.android.libraries.maps.GoogleMap;
import com.google.android.libraries.maps.model.BitmapDescriptorFactory;
import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.MarkerOptions;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class Main extends AppCompatActivity {

    public static Main main;
    public static BaseDatos baseDatos;
    public static final int CODIGO_IDENTIFICADOR = 1;

    public GoogleMap mMap;
    public Location miLocation;
    public ArrayList<MarkerOptions> jugadores = new ArrayList<>();
    public Button buttonClasificacion;
    public Button buttonColeccion;

    public Thread markers = new Thread(){
        @Override
        public void run() {
            while(true) {
                try {
                    if(jugadores.size() > 10) {
                        jugadores.remove(10);
                        if(Mapa.marcadores.size() > 10) {
                            Main.main.runOnUiThread(() -> Mapa.marcadores.get(10).remove());
                        }
                    }
                    double range = 0.007;
                    double minLat = miLocation.getLatitude() - range;
                    double maxLat = miLocation.getLatitude() + range;
                    double minlon = miLocation.getLongitude() - range;
                    double maxlon = miLocation.getLongitude() + range;
                    double latNewMarker = Math.random() * (maxLat - minLat) + minLat;
                    double lonNewMarker = Math.random() * (maxlon - minlon) + minlon;
                    Main.main.runOnUiThread(() -> {
                        jugadores.add(0, new MarkerOptions()
                                .anchor(0.5f, 1f)
                                .draggable(false)
                                .position(new LatLng(latNewMarker, lonNewMarker))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carta))
                                .title(String.valueOf(Math.random() * Integer.MAX_VALUE)));
                        Mapa.marcadores.add(0, mMap.addMarker(jugadores.get(0)));
                    });
                    int segundos = (int)(Math.random() * (30-10) + 10);
                    Thread.sleep(segundos * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        buttonColeccion.setText(getResources().getString(R.string.coleccion) + " " + baseDatos.getJugadoresColeccion());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createDB();
        main = this;
        buttonColeccion = (Button) findViewById(R.id.buttonColeccion);
        buttonColeccion.setEnabled(false);
        buttonClasificacion = (Button) findViewById(R.id.buttonClasificacion);
        buttonClasificacion.setEnabled(false);
        XMLTeams xmlTeams = new XMLTeams(this, getFilesDir() + File.separator + "equipos.xml");
        xmlTeams.execute();
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.layout);
        relativeLayout.setBackgroundResource(R.drawable.fondo);AnimationDrawable frameAnimation = (AnimationDrawable) relativeLayout.getBackground();
        frameAnimation.start();
        if (Build.VERSION.SDK_INT >= 23) {
            int permiso1 = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            int permiso2 = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            if (permiso1 != PackageManager.PERMISSION_GRANTED || permiso2 != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, CODIGO_IDENTIFICADOR);
            }
        }
    }

    private void createDB() {
        String bddestino = "/data/data/" + getPackageName() + "/databases/" + BaseDatos.NOME_BD;
        File file = new File(bddestino);
        if (!file.exists()) {
            System.out.println();
            String pathbd = "/data/data/" + getPackageName() + "/databases";
            File filepathdb = new File(pathbd);
            filepathdb.mkdirs();
            dbFromAssets(bddestino);
        } else {
            baseDatos = new BaseDatos(this);
            baseDatos.sqlLiteDB = baseDatos.getWritableDatabase();
        }
    }

    private void dbFromAssets(String bddestino) {
        try {
            InputStream is = getApplicationContext().getAssets().open(BaseDatos.NOME_BD);
            OutputStream os = new FileOutputStream(bddestino, false);
            int salidaByte;
            while ((salidaByte = is.read()) != -1) os.write(salidaByte);
            os.flush();
            os.close();
            is.close();
            baseDatos = new BaseDatos(this);
            baseDatos.sqlLiteDB = baseDatos.getWritableDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClickMapa(View view) {
        Intent intent = new Intent(this, Mapa.class);
        startActivity(intent);
    }

    public void onClickClasificacion(View view) {
        Intent intent = new Intent(this, Clasificacion.class);
        startActivity(intent);
    }

    public void onClickCardView(int posicion) {
        Intent intent = new Intent(this, EquipoEstadisticas.class);
        intent.putExtra("POSICION", posicion);
        startActivity(intent);
    }

    public void onClickCardViewPlantilla(String idJugadorPlantilla) {
        Intent intent = new Intent(this, JugadorEstadisticas.class);
        intent.putExtra("IDJUGADOR", idJugadorPlantilla);
        startActivity(intent);
    }

    public void onClickColeccion(View view) {
        Intent intent = new Intent(this, Plantilla.class);
        startActivity(intent);
    }
}