package com.example.futbolerosgo;

public class JugadorPlantilla {

    private int calidadPlantilla;
    private String nombrePlantilla;
    private int obtenidoPlantilla;
    private String idJugadorPlantilla;

    public JugadorPlantilla(int calidadPlantilla, String nombrePlantilla, int obtenidoPlantilla, String idJugadorPlantilla) {
        this.calidadPlantilla = calidadPlantilla;
        if(nombrePlantilla.length() > 20) nombrePlantilla = nombrePlantilla.substring(0, 21) + "...";
        this.nombrePlantilla = nombrePlantilla;
        this.obtenidoPlantilla = obtenidoPlantilla;
        this.idJugadorPlantilla = idJugadorPlantilla;
    }

    public int getCalidadPlantilla() {
        return calidadPlantilla;
    }

    public void setCalidadPlantilla(int calidadPlantilla) {
        this.calidadPlantilla = calidadPlantilla;
    }

    public String getNombrePlantilla() {
        return nombrePlantilla;
    }

    public void setNombrePlantilla(String nombrePlantilla) {
        this.nombrePlantilla = nombrePlantilla;
    }

    public int getObtenidoPlantilla() {
        return obtenidoPlantilla;
    }

    public void setObtenidoPlantilla(int obtenidoPlantilla) {
        this.obtenidoPlantilla = obtenidoPlantilla;
    }

    public String getIdJugadorPlantilla() {
        return idJugadorPlantilla;
    }

    public void setIdJugadorPlantilla(String idJugadorPlantilla) {
        this.idJugadorPlantilla = idJugadorPlantilla;
    }
}
