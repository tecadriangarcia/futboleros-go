package com.example.futbolerosgo;

public class SiguientePartido {

    String equipoRival;
    String escudoEquipoRival;
    boolean local;

    public SiguientePartido() {
    }

    public SiguientePartido(String equipoRival, boolean local) {
        this.equipoRival = equipoRival;
        this.local = local;
    }

    @Override
    public String toString() {
        return "SiguientePartido{" +
                "equipoRival='" + equipoRival + '\'' +
                ", local=" + local +
                '}';
    }
}
