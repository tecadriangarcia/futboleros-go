package com.example.futbolerosgo;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolder> {

    private ArrayList<EquipoClasificacion> equipos;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imagen;
        public TextView posicion;
        public TextView nombre;
        public TextView puntos;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagen = itemView.findViewById(R.id.image);
            posicion = itemView.findViewById(R.id.posicion);
            nombre = itemView.findViewById(R.id.nombre);
            puntos = itemView.findViewById(R.id.puntos);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    public Adaptador(ArrayList<EquipoClasificacion> equiposClasificacion) {
        this.equipos = equiposClasificacion;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.equipo_card, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EquipoClasificacion equipo = equipos.get(position);
        boolean blanco = true;
        holder.cardView.setOnClickListener(v -> Main.main.onClickCardView(position+1));
        if(position < 4) {
            holder.cardView.setCardBackgroundColor(0xff1045A5);
        } else if (position == 4) {
            holder.cardView.setCardBackgroundColor(0xff69110b);
        } else if (position == 5) {
            holder.cardView.setCardBackgroundColor(0xffd5ad1c);
        } else if(position > 16) {
            holder.cardView.setCardBackgroundColor(0xffD10E00);
        } else {
            holder.cardView.setCardBackgroundColor(0xffefefef);
            blanco = false;
        }

        holder.nombre.setTextColor(blanco ? 0xffffffff : 0xff000000);
        holder.posicion.setTextColor(blanco ? 0xffffffff : 0xff000000);
        holder.puntos.setTextColor(blanco ? 0xffffffff : 0xff000000);

        holder.imagen.setImageResource(equipo.getmImage());
        holder.nombre.setText(equipo.getEquipo());
        holder.posicion.setText(equipo.getPosicion());;
        holder.puntos.setText(equipo.getPuntos());
    }

    @Override
    public int getItemCount() {
        return equipos.size();
    }
}
