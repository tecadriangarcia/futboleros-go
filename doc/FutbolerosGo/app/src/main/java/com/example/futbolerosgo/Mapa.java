package com.example.futbolerosgo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.google.android.libraries.maps.CameraUpdateFactory;
import com.google.android.libraries.maps.GoogleMap;
import com.google.android.libraries.maps.OnMapReadyCallback;
import com.google.android.libraries.maps.SupportMapFragment;
import com.google.android.libraries.maps.model.BitmapDescriptor;
import com.google.android.libraries.maps.model.BitmapDescriptorFactory;
import com.google.android.libraries.maps.model.CameraPosition;
import com.google.android.libraries.maps.model.Circle;
import com.google.android.libraries.maps.model.CircleOptions;
import com.google.android.libraries.maps.model.LatLng;
import com.google.android.libraries.maps.model.Marker;
import com.google.android.libraries.maps.model.MarkerOptions;
import java.util.ArrayList;

public class Mapa extends AppCompatActivity implements LocationListener, OnMapReadyCallback {

    // mapa 2 - 87cc19f2157727f0

    private Main main;
    private Mapa mapa;
    private ToggleButton toggleButtonBuildings;
    private TextView textViewMonedas;
    private LocationManager locManager;
    private String proveedor;
    private SupportMapFragment mapFragment;
    private LatLng pos;
    private boolean first = true;
    private boolean changeBuildings = false;
    public static ArrayList<Marker> marcadores = new ArrayList<>();
    public Marker posiciónAnterior;
    private float bearing;
    private CameraPosition lastCameraPosition;
    private BitmapDescriptor avatarMap;
    private MarkerOptions markerOptions;
    private Circle areaUser;
    private int zona;
    private int contador = 1;
    private int velocidad = 25;
    private String usuario;

    // ROJO, AZUL, AMARILLO, CIAN, VERDE, GRIS, VIOLETA, MARRON, ROSA, NEGRO

    private int[] colores = {0x40ff0000, 0x400000ff, 0x40ffff00, 0x4000ffff, 0x4000ff00
            ,0x409e9e9e, 0x40ff00ff, 0x40903000, 0x50ffb1d2, 0x70222222};

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) velocidad = -1;
        return super.onKeyDown(keycode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
        mapa = this;
        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        toggleButtonBuildings = (ToggleButton) findViewById(R.id.buttonBuildings);
        textViewMonedas = (TextView) findViewById(R.id.textViewMonedas);
        String monedas = preferencias.getInt("MONEDAS", 0) + "";
        textViewMonedas.setText(monedas);
        textViewMonedas.setOnClickListener(v -> Toast.makeText(mapa, getResources().getString(R.string.tienesMonedas) + " " + textViewMonedas.getText().toString() + " " + getResources().getString(R.string.tienesMonedas2), Toast.LENGTH_SHORT).show());
        usuario = "usuario";
        marcadores.clear();
        main = Main.main;
        avatarMap = BitmapDescriptorFactory.fromResource(R.drawable.avatar);
        markerOptions = new MarkerOptions()
                .anchor(0.5f, 1f)
                .draggable(false)
                .title(usuario)
                .icon(avatarMap);
        if (Build.VERSION.SDK_INT >= 23) {
            int permiso1 = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            int permiso2 = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            if (permiso1 != PackageManager.PERMISSION_GRANTED || permiso2 != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.necesitasPermisos), Toast.LENGTH_LONG).show();
                finish();
            } else {
                obtenerProvedores();
                mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
            }
        } else {
            obtenerProvedores();
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    private void obtenerProvedores() {
        Criteria filtro = new Criteria();
        filtro.setAccuracy(Criteria.ACCURACY_FINE);

        locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        proveedor = locManager.getBestProvider(filtro, false);

        if (proveedor == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.noProveedores), Toast.LENGTH_SHORT).show();
            finish();
        }
        if (!locManager.isProviderEnabled(proveedor)) {
            dialogoAlertaNoGPS();
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locManager.requestLocationUpdates(proveedor, 0, 5, this);
                Location location = locManager.getLastKnownLocation(proveedor);
                if (location == null) pos = new LatLng(0, 0);
                else pos = new LatLng(location.getLatitude(), location.getLongitude());
                zona = getZona(pos.latitude, pos.longitude);
            }
        }
    }

    private void dialogoAlertaNoGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(
                getResources().getString(R.string.noGps))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes),
                        (arg0, arg1) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton(getResources().getString(R.string.no), (dialog, which) -> {
                    dialog.cancel();
                    Mapa.this.finish();
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location.getSpeed() <= velocidad) {
            main.miLocation = location;
            pos = new LatLng(location.getLatitude(), location.getLongitude());
            zona = getZona(location.getLatitude(), location.getLongitude());
            Log.e("zona", zona+"");
            bearing = location.getBearing();
            if (contador > 2) {
                SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = preferencias.edit();
                int monedas = preferencias.getInt("MONEDAS", 0) + 5;
                textViewMonedas.setText(String.valueOf(monedas));
                editor.putInt("MONEDAS", monedas);
                editor.apply();
            } else contador++;
            mapFragment.getMapAsync(this);
        }
    }

    public static int getZona(double latitud, double longitud) {
        int lat = Math.abs((int)(((int)latitud - latitud) * 100));
        int lon = Math.abs((int)(((int)longitud - longitud) * 100));

        int lat1;
        int lat2;
        if(lat < 10){
            lat1 = 0;
            lat2 = lat;
        } else {
            lat1 = lat / 10;
            lat2 = lat % 10;
        }

        int lon1;
        int lon2;
        if(lon < 10){
            lon1 = 0;
            lon2 = lon;
        } else {
            lon1 = lon / 10;
            lon2 = lon % 10;
        }

        if((lat1 + lon1) % 2 == 0 ) {
            int suma = lat2 + lon2;
            return suma > 9 ? suma - ((suma-9) * 2) : suma;
        } else {
            return lat2 - lon2 < 0 ? lat2 + Math.abs(lon2-9) : lon2 + Math.abs(lat2-9);
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.noActiveProvider), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.activeProvider), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onClickBuildings(View view) {
        actionBuildingsChanged();
    }

    public void actionBuildingsChanged() {
        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferencias.edit();
        if(preferencias.getBoolean("BUILDINGS", true)) editor.putBoolean("BUILDINGS", false);
        else editor.putBoolean("BUILDINGS", true);
        changeBuildings = true;
        mapFragment.getMapAsync(this);
        editor.apply();
    }

    public void onClickResetPosicion(View view) {
        CameraPosition cameraPosition1 = new CameraPosition.Builder(lastCameraPosition)
                .bearing(bearing)
                .target(pos)
                .zoom(17.5f)
                .build();
        main.mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap == null) {
            Toast.makeText(this, getResources().getString(R.string.errorMap), Toast.LENGTH_SHORT).show();
            finish();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(main.miLocation != null && !main.markers.isAlive()) {
            main.markers.start();
        }
        if (first) {
            main.mMap = googleMap;
            first = false;
            for(MarkerOptions mo : main.jugadores) marcadores.add(0, main.mMap.addMarker(mo));
            Mapa mapa = this;
            main.mMap.setOnMarkerClickListener(marker -> {
                if(!marker.getTitle().equals(usuario)) {
                    float[] distancia = {100f};
                    Location.distanceBetween(posiciónAnterior.getPosition().latitude, posiciónAnterior.getPosition().longitude,
                            marker.getPosition().latitude, marker.getPosition().longitude, distancia);
                    if(distancia[0] < 100) {
                        int monedas = Integer.parseInt(textViewMonedas.getText().toString());
                        CameraPosition cameraPosition = new CameraPosition.Builder(lastCameraPosition)
                                .bearing(bearing)
                                .target(marker.getPosition())
                                .zoom(18.5f)
                                .build();
                        main.mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        AlertDialog.Builder ventana = new AlertDialog.Builder(mapa);
                        ventana.setIcon(R.drawable.carta);
                        ventana.setTitle(getResources().getString(R.string.sobreDeZona) + " " + zona);
                        ArrayList<String> equiposZona = Main.baseDatos.getEquiposZona(getZona(marker.getPosition().latitude, marker.getPosition().longitude));
                        if(monedas < 250) {
                            ventana.setMessage(getResources().getString(R.string.tienesMonedas) + " " + textViewMonedas.getText().toString() + " " +  getResources().getString(R.string.no250monedas) + " (" + equiposZona.get(0) +", " + equiposZona.get(1) + ")");
                            ventana.setNegativeButton(getResources().getString(R.string.volver), (dialog, boton) -> {
                                CameraPosition cameraPosition1 = new CameraPosition.Builder(lastCameraPosition)
                                        .bearing(bearing)
                                        .target(pos)
                                        .zoom(17.5f)
                                        .build();
                                main.mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));
                            });
                        } else {
                            ventana.setMessage(getResources().getString(R.string.tienesMonedas) + " " +  textViewMonedas.getText().toString() + " " +  getResources().getString(R.string.yes250monedas) + " (" + equiposZona.get(0) +", " + equiposZona.get(1) + ")");
                            ventana.setCancelable(false);
                            ventana.setPositiveButton(getResources().getString(R.string.yes), (dialog, boton) -> {
                                SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = preferencias.edit();
                                String monedasActuales = monedas-250 + "";
                                textViewMonedas.setText(monedasActuales);
                                editor.putInt("MONEDAS", monedas-250);
                                editor.apply();
                                CameraPosition cameraPosition1 = new CameraPosition.Builder(lastCameraPosition)
                                        .bearing(bearing)
                                        .target(pos)
                                        .zoom(17.5f)
                                        .build();
                                main.mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));
                                for(int i = 0 ; i < main.jugadores.size() ; i++) {
                                    if (marker.getTitle().equals(main.jugadores.get(i).getTitle())) {
                                        main.jugadores.remove(i);
                                        marcadores.get(i).remove();
                                        marcadores.remove(i);
                                        marker.remove();
                                        break;
                                    }
                                }
                                Toast.makeText(mapa, Main.baseDatos.getRandomPlayerFromZone(zona), Toast.LENGTH_LONG).show();
                            });
                            ventana.setNegativeButton(getResources().getString(R.string.no), (dialog, boton) -> {
                                CameraPosition cameraPosition1 = new CameraPosition.Builder(lastCameraPosition)
                                        .bearing(bearing)
                                        .target(pos)
                                        .zoom(17.5f)
                                        .build();
                                main.mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));
                            });
                        }
                        ventana.create().show();
                    }
                }
                return true;
            });
            SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            boolean buildingsVisible = preferencias.getBoolean("BUILDINGS", true);
            main.mMap.setBuildingsEnabled(buildingsVisible);
            toggleButtonBuildings.setChecked(buildingsVisible);
            toggleButtonBuildings.setCompoundDrawablesRelativeWithIntrinsicBounds(null, buildingsVisible ? getResources().getDrawable(R.drawable.buildingstrue, null) : getResources().getDrawable(R.drawable.buildingsfalse, null), null, null);
            lastCameraPosition = main.mMap.getCameraPosition();
            main.mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            main.mMap.setMaxZoomPreference(18.5f);
            main.mMap.setMinZoomPreference(17.5f);
            main.mMap.getUiSettings().setAllGesturesEnabled(false);
            main.mMap.getUiSettings().setRotateGesturesEnabled(true);
            //main.mMap.getUiSettings().setZoomGesturesEnabled(true);
            main.mMap.getUiSettings().setCompassEnabled(false);
        } else if(changeBuildings) {
            main.mMap = googleMap;
            changeBuildings = false;
            SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            boolean buildingsVisible = preferencias.getBoolean("BUILDINGS", true);
            toggleButtonBuildings.setChecked(buildingsVisible);
            toggleButtonBuildings.setCompoundDrawablesRelativeWithIntrinsicBounds(null, buildingsVisible ? getResources().getDrawable(R.drawable.buildingstrue, null) : getResources().getDrawable(R.drawable.buildingsfalse, null), null, null);
            main.mMap.setBuildingsEnabled(buildingsVisible);
            return;
        } else {
            main.mMap = googleMap;
            lastCameraPosition = main.mMap.getCameraPosition();
            posiciónAnterior.remove();
            areaUser.remove();
        }
        markerOptions.position(pos);
        CameraPosition cameraPosition = new CameraPosition.Builder(lastCameraPosition)
                .bearing(bearing)
                .target(pos)
                .build();
        posiciónAnterior = main.mMap.addMarker(markerOptions);
        areaUser = main.mMap.addCircle(new CircleOptions().center(pos).fillColor(colores[zona]).radius(100).strokeWidth(2).strokeColor(Color.BLACK));
        main.mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

}