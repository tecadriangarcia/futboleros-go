package com.example.futbolerosgo;

public class EquipoClasificacion {

    private int mImage;
    private String posicion;
    private String equipo;
    private String puntos;

    public EquipoClasificacion(int mImage, String posicion, String equipo, String puntos) {
        this.mImage = mImage;
        this.posicion = posicion.length() < 2 ? 0 + posicion : posicion;
        this.equipo = equipo;
        this.puntos = puntos;
    }

    public int getmImage() {
        return mImage;
    }

    public void setmImage(int mImage) {
        this.mImage = mImage;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public String getPuntos() {
        return puntos;
    }

    public void setPuntos(String puntos) {
        this.puntos = puntos;
    }
}
