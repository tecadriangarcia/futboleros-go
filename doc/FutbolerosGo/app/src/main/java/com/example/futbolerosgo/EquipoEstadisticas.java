package com.example.futbolerosgo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class EquipoEstadisticas extends AppCompatActivity {

    private ImageView escudo;
    private TextView tvNombre;
    private TextView tvPosicion;
    private TextView tvPuntos;
    private TextView tvPjugados;
    private TextView tvPGanados;
    private TextView tvPEmpatados;
    private TextView tvPPerdidos;
    private TextView tvGFavor;
    private TextView tvGContra;
    /*private TextView tvSPartido;
    private TextView tvSPartidoLocal;
    private TextView tvSPartidoVis;*/

    private TextView tvsUltimos5;
    private TextView tvUPartidoLocal1;
    private TextView tvUPartidoVis1;
    private TextView tvUPartidoLocal2;
    private TextView tvUPartidoVis2;
    private TextView tvUPartidoLocal3;
    private TextView tvUPartidoVis3;
    private TextView tvUPartidoLocal4;
    private TextView tvUPartidoVis4;
    private TextView tvUPartidoLocal5;
    private TextView tvUPartidoVis5;

    private Drawable equipoDrawable;

    private String nombreEquipo;

    private Button botonSquat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipo_estadisticas);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        this.escudo = (ImageView) findViewById(R.id.imagenEquipoHistorialEquipoEst);
        this.tvNombre = (TextView) findViewById(R.id.nombreEquipoEquipoEst);
        this.tvPosicion = (TextView) findViewById(R.id.posicionEquipoEst);
        this.tvPuntos = (TextView) findViewById(R.id.puntosEquipoEst);
        this.tvPjugados = (TextView) findViewById(R.id.textViewPJEquipoEst);
        this.tvPGanados = (TextView) findViewById(R.id.partidosGanadosEquipoEst);
        this.tvPEmpatados = (TextView) findViewById(R.id.partidosEmpatadosEquipoEst);
        this.tvPPerdidos = (TextView) findViewById(R.id.partidosPerdidosEquipoEst);
        this.tvGFavor = (TextView) findViewById(R.id.golesFavorEquipoEst);
        this.tvGContra = (TextView) findViewById(R.id.golesContraEquipoEst);
        /*this.tvSPartido = (TextView) findViewById(R.id.textViewsSiguientePartidoEquipoEst);
        this.tvSPartidoLocal = (TextView) findViewById(R.id.siguientePartidoLocalEquipoEst);
        this.tvSPartidoVis = (TextView) findViewById(R.id.siguientePartidoVisEquipoEst);*/
        this.tvsUltimos5 = (TextView) findViewById(R.id.textViewsUltimos5Partidos);
        this.tvUPartidoLocal1 = (TextView) findViewById(R.id.ultimoPartido1Local);
        this.tvUPartidoVis1 = (TextView) findViewById(R.id.ultimoPartido1Vis);
        this.tvUPartidoLocal2 = (TextView) findViewById(R.id.ultimoPartido2Local);
        this.tvUPartidoVis2 = (TextView) findViewById(R.id.ultimoPartido2Vis);
        this.tvUPartidoLocal3 = (TextView) findViewById(R.id.ultimoPartido3Local);
        this.tvUPartidoVis3 = (TextView) findViewById(R.id.ultimoPartido3Vis);
        this.tvUPartidoLocal4 = (TextView) findViewById(R.id.ultimoPartido4Local);
        this.tvUPartidoVis4 = (TextView) findViewById(R.id.ultimoPartido4Vis);
        this.tvUPartidoLocal5 = (TextView) findViewById(R.id.ultimoPartido5Local);
        this.tvUPartidoVis5 = (TextView) findViewById(R.id.ultimoPartido5Vis);

        this.botonSquat = (Button) findViewById(R.id.buttonPlantillasEquipoEst);
        int posicion = getIntent().getExtras().getInt("POSICION");
        Equipo equipo = Main.baseDatos.getEquipoEstadisticas(posicion);

        botonSquat.append(" " + Main.baseDatos.getJugadoresColeccion(equipo.nombre));

        nombreEquipo = equipo.nombre;

        Drawable equipoLocal = getResources().getDrawable(getResources().getIdentifier(equipo.escudo, "drawable", getApplicationContext().getPackageName()), null);
        Bitmap bitmap = ((BitmapDrawable) equipoLocal).getBitmap();
        equipoDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 60, 60, true));

        escudo.setImageResource(getResources().getIdentifier(equipo.escudo, "drawable", getApplicationContext().getPackageName()));
        tvNombre.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" + equipo.nombre + "</b></span>"));
        tvNombre.append("\n");
        tvPosicion.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" + getString(R.string.posicion) + " " + equipo.posicion + "º</b></span>"));
        tvPuntos.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" + getString(R.string.puntos) + " " + equipo.puntos + "</b></span>"));
        tvPjugados.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" + getString(R.string.partidosJugados) + "</b></span>"));
        tvPjugados.append("\n" + equipo.partidosJugados);
        tvPGanados.setText(Html.fromHtml("<span style=\"color:#106000;\"><b>" + getString(R.string.partidosGanados) + "</b></span>"));
        tvPGanados.append("\n" + equipo.partidosGanados);
        tvPEmpatados.setText(Html.fromHtml("<span><b>" + getString(R.string.partidosEmpatados) + "</b></p>"));
        tvPEmpatados.append("\n" + equipo.partidosEmpatados);
        tvPPerdidos.setText(Html.fromHtml("<span style=\"color:#ff1010;\"><b>" + getString(R.string.partidosPerdidos) + "</b></span>"));
        tvPPerdidos.append("\n" + equipo.partidosPerdidos);
        tvGFavor.setText(Html.fromHtml("<span style=\"color:#106000;\"><b>" + getString(R.string.golesFavor) + " ~ </b></span>" + equipo.golesAFavor));
        tvGContra.setText(Html.fromHtml(equipo.golesEnContra + "<span style=\"color:#ff1010;\"><b> ~ " + getString(R.string.golesContra) + "</b></span>"));
        /*tvSPartido.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" + getString(R.string.siguientePartido) + "</b></span>"));
        if(equipo.siguientePartido.local) {
            tvSPartidoLocal.setText(Html.fromHtml("<span style=\"color:#000000;\">" + equipo.nombre + "</span>"));
            tvSPartidoVis.setText(Html.fromHtml("<span style=\"color:#000000;\">" + equipo.siguientePartido.equipoRival + "</span>"));

            tvSPartidoLocal.setCompoundDrawablesRelativeWithIntrinsicBounds(equipoDrawable, null, null, null);

            Drawable equipoVis1 = getResources().getDrawable(getResources().getIdentifier(equipo.siguientePartido.escudoEquipoRival, "drawable", getApplicationContext().getPackageName()), null);
            Bitmap bitmap2 = ((BitmapDrawable) equipoVis1).getBitmap();
            Drawable equipoVis2 = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap2, 60, 60, true));
            tvSPartidoVis.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, equipoVis2, null);
        } else {
            tvSPartidoLocal.setText(Html.fromHtml("<span style=\"color:#000000;\">" + equipo.siguientePartido.equipoRival + "</span>"));
            tvSPartidoVis.setText(Html.fromHtml("<span style=\"color:#000000;\">" + equipo.nombre + "</span>"));

            Drawable equipoLocal1 = getResources().getDrawable(getResources().getIdentifier(equipo.siguientePartido.escudoEquipoRival, "drawable", getApplicationContext().getPackageName()), null);
            Bitmap bitmap1 = ((BitmapDrawable) equipoLocal1).getBitmap();
            Drawable equipoLocal2 = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap1, 60, 60, true));
            tvSPartidoLocal.setCompoundDrawablesRelativeWithIntrinsicBounds(equipoLocal2, null, null, null);

            tvSPartidoVis.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, equipoDrawable, null);
        }*/
        tvsUltimos5.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" + getString(R.string.ultimos5partidos) + "</b></span>"));
        generateUltimoPartido(equipo, equipo.ultimosPartidos.get(0), tvUPartidoLocal1, tvUPartidoVis1);
        generateUltimoPartido(equipo, equipo.ultimosPartidos.get(1), tvUPartidoLocal2, tvUPartidoVis2);
        generateUltimoPartido(equipo, equipo.ultimosPartidos.get(2), tvUPartidoLocal3, tvUPartidoVis3);
        generateUltimoPartido(equipo, equipo.ultimosPartidos.get(3), tvUPartidoLocal4, tvUPartidoVis4);
        generateUltimoPartido(equipo, equipo.ultimosPartidos.get(4), tvUPartidoLocal5, tvUPartidoVis5);
    }

    public void generateUltimoPartido(Equipo equipo, UltimoPartido ultimoPartido, TextView local, TextView vis) {
        int numeroGolesFavor = Integer.parseInt(ultimoPartido.golesAFavor);
        int numeroGolesContra = Integer.parseInt(ultimoPartido.golesEnContra);
        if(ultimoPartido.local) {

            local.setText(equipo.nombre);
            local.append("\n" + ultimoPartido.golesAFavor);
            vis.setText(ultimoPartido.equipoRival);
            vis.append("\n" + ultimoPartido.golesEnContra);

            if(numeroGolesContra < numeroGolesFavor) {
                local.setTextColor(0xff106000);
                vis.setTextColor(0xffff1010);
            } else if (numeroGolesContra > numeroGolesFavor) {
                local.setTextColor(0xffff1010);
                vis.setTextColor(0xff106000);
            }

            local.setCompoundDrawablesRelativeWithIntrinsicBounds(equipoDrawable, null, null, null);

            Drawable equipoVis1 = getResources().getDrawable(getResources().getIdentifier(ultimoPartido.escudoEquipoRival, "drawable", getApplicationContext().getPackageName()), null);
            Bitmap bitmap2 = ((BitmapDrawable) equipoVis1).getBitmap();
            Drawable equipoVis2 = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap2, 60, 60, true));
            vis.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, equipoVis2, null);

        } else {

            local.setText(ultimoPartido.equipoRival);
            local.append("\n" + ultimoPartido.golesEnContra);
            vis.setText(equipo.nombre);
            vis.append("\n" + ultimoPartido.golesAFavor);

            if(numeroGolesContra > numeroGolesFavor) {
                local.setTextColor(0xff106000);
                vis.setTextColor(0xffff1010);
            } else if (numeroGolesContra < numeroGolesFavor) {
                local.setTextColor(0xffff1010);
                vis.setTextColor(0xff106000);
            }

            Drawable equipoLocal1 = getResources().getDrawable(getResources().getIdentifier(ultimoPartido.escudoEquipoRival, "drawable", getApplicationContext().getPackageName()), null);
            Bitmap bitmap1 = ((BitmapDrawable) equipoLocal1).getBitmap();
            Drawable equipoLocal2 = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap1, 60, 60, true));
            local.setCompoundDrawablesRelativeWithIntrinsicBounds(equipoLocal2, null, null, null);

            vis.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, equipoDrawable, null);
        }
    }

    public void onClickPlantilla(View view) {
        Intent intent = new Intent(this, Plantilla.class);
        intent.putExtra("EQUIPO", nombreEquipo);
        startActivity(intent);
    }

}