package com.example.futbolerosgo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

class URLAsync extends AsyncTask<Void, Integer, Boolean> {

    private JugadorEstadisticas jugadorEstadisticas;
    private String cromoPlantilla;

    public URLAsync(JugadorEstadisticas jugadorEstadisticas, String cromoPlantilla) {
        this.jugadorEstadisticas = jugadorEstadisticas;
        this.cromoPlantilla = cromoPlantilla;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        URL url = null;
        try {
            if (Locale.getDefault().getDisplayLanguage().equals("español")) {
                url = new URL("https://a19adriangces.weebly.com/uploads/1/3/6/2/136297613/" + cromoPlantilla);
            } else {
                url = new URL("https://a19adriangcen.weebly.com/uploads/1/3/6/3/136300045/" + cromoPlantilla);
            }
            Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            jugadorEstadisticas.runOnUiThread(() -> {
                jugadorEstadisticas.cromoJugadorEst.setImageBitmap(bitmap);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}

public class JugadorEstadisticas extends AppCompatActivity {

    public static String[] skillsJugador;
    public static String[] skillsPortero;
    public String[] posiciones;

    public ImageView cromoJugadorEst;

    private TextView nombreJugadorEst;
    private TextView posicionJugadorEst;
    private TextView calidadJugadorEst;
    private TextView fechaJugadorEst;

    private TextView skill1JugadorEst;
    private TextView skill2JugadorEst;
    private TextView skill3JugadorEst;
    private TextView skill4JugadorEst;
    private TextView skill5JugadorEst;
    private TextView skill6JugadorEst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugador_estadisticas);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        String idJugadorDB = getIntent().getExtras().getString("IDJUGADOR");
        Jugador jugador = Main.baseDatos.getJugadorEstadisticas(idJugadorDB);
        ArrayList<Integer> skillNumbersJugador = Main.baseDatos.getJugadorEstadisticasSkills(idJugadorDB, jugador.getIdPosicion());

        posiciones = getResources().getStringArray(R.array.posiciones);
        skillsJugador = getResources().getStringArray(R.array.skillsJugador);
        skillsPortero = getResources().getStringArray(R.array.skillsPortero);

        cromoJugadorEst = (ImageView) findViewById(R.id.imageViewJugadorEst);

        if(savedInstanceState != null) {
            cromoJugadorEst.setImageBitmap(savedInstanceState.getParcelable("BITMAP"));
        } else {
            URLAsync urlAsync = new URLAsync(this, jugador.getCromo());
            urlAsync.execute();
        }

        nombreJugadorEst = (TextView) findViewById(R.id.nombreJugadorEst);
        calidadJugadorEst = (TextView) findViewById(R.id.calidadJugadorEst);
        posicionJugadorEst = (TextView) findViewById(R.id.posicionJugadorEst);
        fechaJugadorEst = (TextView) findViewById(R.id.fechaJugadorEst);
        skill1JugadorEst = (TextView) findViewById(R.id.skill1JugadorEst);
        skill2JugadorEst = (TextView) findViewById(R.id.skill2JugadorEst);
        skill3JugadorEst = (TextView) findViewById(R.id.skill3JugadorEst);
        skill4JugadorEst = (TextView) findViewById(R.id.skill4JugadorEst);
        skill5JugadorEst = (TextView) findViewById(R.id.skill5JugadorEst);
        skill6JugadorEst = (TextView) findViewById(R.id.skill6JugadorEst);

        nombreJugadorEst.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" + jugador.getNombre() + "</b></span>"));
        nombreJugadorEst.append("\n");

        fechaJugadorEst.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" + jugador.getFechaNacimiento() + "</b></span>"));
        fechaJugadorEst.append("\n");

        calidadJugadorEst.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>"+ getString(R.string.calidad) + "-</b>" + jugador.getCalidad() + "</span>"));

        posicionJugadorEst.setText(Html.fromHtml("<span style=\"color:#000000;\"><b>" +  getString(R.string.posicion) + "-</b>" + posiciones[jugador.getIdPosicion()-1] + "</span>"));

        if (jugador.getIdPosicion() == 1) {
            skill1JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(0) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(0) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsPortero[0] + "</b></span>"));
            skill1JugadorEst.append("\n" + skillNumbersJugador.get(0));
            skill2JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(1) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(1) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsPortero[1] + "</b></span>"));
            skill2JugadorEst.append("\n" + skillNumbersJugador.get(1));
            skill3JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(2) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(2) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsPortero[2] + "</b></span>"));
            skill3JugadorEst.append("\n" + skillNumbersJugador.get(2));
            skill4JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(3) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(3) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsPortero[3] + "</b></span>"));
            skill4JugadorEst.append("\n" + skillNumbersJugador.get(3));
            skill5JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(4) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(4) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsPortero[4] + "</b></span>"));
            skill5JugadorEst.append("\n" + skillNumbersJugador.get(4));
            skill6JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(5) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(5) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsPortero[5] + "</b></span>"));
            skill6JugadorEst.append("\n" + skillNumbersJugador.get(5));
        } else {
            skill1JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(0) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(0) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsJugador[0] + "</b></span>"));
            skill1JugadorEst.append("\n" + skillNumbersJugador.get(0));
            skill2JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(1) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(1) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsJugador[1] + "</b></span>"));
            skill2JugadorEst.append("\n" + skillNumbersJugador.get(1));
            skill3JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(2) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(2) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsJugador[2] + "</b></span>"));
            skill3JugadorEst.append("\n" + skillNumbersJugador.get(2));
            skill4JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(3) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(3) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsJugador[3] + "</b></span>"));
            skill4JugadorEst.append("\n" + skillNumbersJugador.get(3));
            skill5JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(4) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(4) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsJugador[4] + "</b></span>"));
            skill5JugadorEst.append("\n" + skillNumbersJugador.get(4));
            skill6JugadorEst.setText(Html.fromHtml("<span" + (skillNumbersJugador.get(5) < 65 ? " style=\"color:#ff1010;\">" : skillNumbersJugador.get(5) < 75 ? ">" : " style=\"color:#106000;\">") +"<b>" + skillsJugador[5] + "</b></span>"));
            skill6JugadorEst.append("\n" + skillNumbersJugador.get(5));
        }
    }

}