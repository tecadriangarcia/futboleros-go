package com.example.futbolerosgo;

import java.util.ArrayList;

public class Equipo {

    public String posicion;
    public String nombre;
    public String partidosJugados;
    public String partidosGanados;
    public String partidosEmpatados;
    public String partidosPerdidos;
    public String golesAFavor;
    public String golesEnContra;
    public String puntos;
    //public SiguientePartido siguientePartido;
    public ArrayList<UltimoPartido> ultimosPartidos;
    public String zona;
    public String escudo;

    public Equipo() {
    }

    public Equipo(ArrayList<String> datos, ArrayList<UltimoPartido> ultimosPartidos) {
        this.posicion = datos.get(0);
        this.nombre = datos.get(1);
        this.partidosJugados = datos.get(2);
        this.partidosGanados = datos.get(3);
        this.partidosEmpatados = datos.get(4);
        this.partidosPerdidos = datos.get(5);
        this.golesAFavor = datos.get(6);
        this.golesEnContra = datos.get(7);
        this.puntos = datos.get(8);
        //this.siguientePartido = siguientePartido;
        this.ultimosPartidos = (ArrayList<UltimoPartido>) ultimosPartidos.clone();
    }

}
