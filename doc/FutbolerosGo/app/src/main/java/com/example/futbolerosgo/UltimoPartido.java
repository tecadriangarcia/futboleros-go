package com.example.futbolerosgo;

public class UltimoPartido {

    String equipoRival;
    String escudoEquipoRival;
    String nPartido;
    boolean local;
    String golesAFavor;
    String golesEnContra;

    public UltimoPartido() {
    }

    public UltimoPartido(String equipoRival, String nPartido, boolean local, String goles1, String goles2) {
        this.equipoRival = equipoRival;
        this.nPartido = nPartido;
        this.local = local;
        if (local) {
            this.golesAFavor = goles1;
            this.golesEnContra = goles2;
        } else {
            this.golesAFavor = goles2;
            this.golesEnContra = goles1;
        }
    }

    @Override
    public String toString() {
        return "UltimoPartido{" +
                "equipoRival='" + equipoRival + '\'' +
                ", nPartido='" + nPartido + '\'' +
                ", local=" + local +
                ", golesAFavor='" + golesAFavor + '\'' +
                ", golesEnContra='" + golesEnContra + '\'' +
                '}';
    }
}
