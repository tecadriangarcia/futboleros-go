package com.example.futbolerosgo;

import android.os.AsyncTask;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XMLTeams extends AsyncTask<Void, Integer, Boolean> {

    private Main parent;
    private String rutaCompleta;
    //private String enlace = "https://es.fcstats.com/tabla,primera-division-espana,19,1.php";
    private String enlace = "https://fcstats.com/table,primera-division-spain,19,1.php";

    public XMLTeams(Main parent, String rutaCompleta) {
        this.parent = parent;
        this.rutaCompleta = rutaCompleta;
    }

    int[] zonas = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 9, 9, 5, 5, 6, 6, 7, 7, 8, 8};

    @Override
    public Boolean doInBackground(Void... params) {

        String line;
        boolean tBody = false;
        int contadorEquipo = 0;
        ArrayList<String> datosEquipo = new ArrayList<>();
        ArrayList<UltimoPartido> ultimosPartidos = new ArrayList<>();
        //SiguientePartido siguientePartido = null;

        try {
            URL url = new URL(enlace);
            InputStream is = url.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String equipo = "";
            while ((line = br.readLine()) != null) {

                line = line.trim();

                if (line.matches("</?tbody>$")) {
                    if (tBody) break;
                    tBody = !tBody;
                }

                if (tBody) {
                    if (contadorEquipo == 14) {
                        contadorEquipo = 0;
                        equipo = "";
                        Equipo tempEquipo = new Equipo(datosEquipo, ultimosPartidos);
                        Main.baseDatos.addTeamInfo(tempEquipo, zonas[Integer.valueOf(tempEquipo.posicion)-1]);
                        datosEquipo.clear();
                        ultimosPartidos.clear();
                    } else {
                        Pattern pattern = Pattern.compile("php\">(.*?)</a></td>");
                        Matcher matcher = pattern.matcher(line);

                        if (matcher.find()) {
                            contadorEquipo++;
                            String equipoHTML = matcher.group(1);
                            datosEquipo.add(equipoHTML);
                            equipo = equipoHTML;
                        }

                        pattern = Pattern.compile(">(\\d+)<");
                        matcher = pattern.matcher(line);
                        if (matcher.find()) {
                            contadorEquipo++;
                            datosEquipo.add(matcher.group(1));
                        }

                        pattern = Pattern.compile("title=\"(.*?)\">$");
                        matcher = pattern.matcher(line);
                        if (matcher.find()) {
                            contadorEquipo++;
                            String partido = matcher.group(1);
                            pattern = Pattern.compile(equipo + "\\s-\\s(.*?)(\\s\\d\\d?:\\d\\d?)?$");
                            matcher = pattern.matcher(partido);
                            String equipoRival;
                            if (matcher.find()) {
                                equipoRival = matcher.group(1);
                                pattern = Pattern.compile(equipoRival + "\\s(.*?):(.*?)$");
                                matcher = pattern.matcher(partido);
                                if (matcher.find()) {
                                    ultimosPartidos.add(new UltimoPartido(equipoRival, Integer.toString(contadorEquipo-9), true, matcher.group(1), matcher.group(2)));
                                } /*else {
                                        siguientePartido = new SiguientePartido(equipoRival, true);
                                }*/
                            } else {
                                pattern = Pattern.compile("(.*?)\\s-\\s");
                                matcher = pattern.matcher(partido);
                                if (matcher.find()) {
                                    equipoRival = matcher.group(1);
                                    pattern = Pattern.compile(equipo + "\\s(.*?):(.*?)$");
                                    matcher = pattern.matcher(partido);
                                    if (matcher.find()) {
                                        ultimosPartidos.add(new UltimoPartido(equipoRival, Integer.toString(contadorEquipo-9), false, matcher.group(1), matcher.group(2)));
                                    } /*else {
                                        siguientePartido = new SiguientePartido(equipoRival, true);
                                    }*/
                                }
                            }
                        }
                    }
                }
            }
            is.close();
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        parent.buttonClasificacion.setEnabled(true);
        parent.buttonColeccion.setEnabled(true);
    }
}