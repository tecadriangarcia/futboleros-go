# Futboleros Go (Deprecated)

## Descripción

Una aplicación que junta coleccionismo y fútbol. Mientras el jugador anda por el mundo se irán apareciendo cromos de jugadoros de fútbol de La Liga. Se consiguen monedas cuando se anda y con 250 de estas, si estas a rango de un jugador, puedes canjearlo y ver que te ha salido. Una vez obtenido un jugador podemos ver datos del mismo en nuestra colección. También estarán disponibles las estadísticas actualizadas de los equipos de La Liga.

>La sección de _"siguiente partido"_ que habría en las estadísticas de los equipos no aparece, aunque tenga su tabla en la base de datos y aparezca en los diagramas, ya que la liga ha terminado.

Las partes complicadas del proyecto tendrán sus propios documentos explicativos.

1. - [Zonas del mapa](doc/files/zonas.md)
2. - [Carga de datos](doc/files/carga.md)
3. - [Cromos](doc/files/cromos.md)

## Instalación / Puesta en marcha

Se puedo probar la aplicación a través de un emulador android o un teléfono real instalando el [apk de lanzamiento](doc/FutbolerosGo/app/release/FutbolerosGo.apk). También se puede descargar el [proyecto completo](doc/FutbolerosGo).

## Uso

La aplicación tiene un uso muy sencillo. Pulsando en el botón play vemos el mapa y como nos movemos. Y además desde el menú inicial podemos ver nuestra colección o la la clasificación de los equipos. Si pulsamos en equipos o jugadores veremos sus datos.

## Sobre el autor

Saludos! Mi nombre es Adrián García Castro. He estudiado el ciclo medio de sistemas microinformáticos y redes y el ciclo superior de desarrollo de aplicaciones multiplataforma. He elegido realizar esta aplicación porque supuso un reto inicialmente, muchas cosas no sabía si podría hacerlas y me gustan los retos y darle al coco.

Para contactar conmigo hacerlo a través del correo tecadriangarcia@gmail.com

## Índice

1. Anteproyecto
    * 1.1. [Idea](doc/templates/1_idea.md)
    * 1.2. [Necesidades](doc/templates/2_necesidades.md)
2. [Diseño](doc/templates/3_diseño.md)
3. [Proyecto](doc/Futboleros%20Go/FutbolerosGo)
4. [Prototipos](doc/templates/4_prototipos.md)


## Guía de contribución

Contribuir es muy sencillo. Cualquiera puede descargar el código y informar de posibles mejoras (que seguro que hay muchas). También son bienvenidas nuevas ideas. Si le vendes la idea a Tevas, invítame a pipas.
